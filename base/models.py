import ldap
import ldap.modlist as modlist
import logging

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

logger = logging.getLogger(__name__)


class User(AbstractUser):
    display_name = models.CharField(max_length=64, blank=True)
    dn = models.CharField(max_length=256, blank=True)

    def change_password(self, old_password, new_password):
        logger.debug("User change password here!")
        #ld = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
