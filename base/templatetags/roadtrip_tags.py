from django import template
from django.contrib.auth import get_user_model
from django_roadtrip.models import RoadTripProfile

register = template.Library()


@register.simple_tag
def roadtrip_default_profile():
    try:
        return RoadTripProfile.objects.filter(user__is_staff=True).first()
    except Exception:
        return None
