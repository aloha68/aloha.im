from libaloha.django import AlohaView, AlohaProtectedView, AlohaMarkdownView
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse

import logging
logger = logging.getLogger(__name__)


class IndexView(AlohaMarkdownView):
    title = 'Bienvenue sur mon site :-)'

    def get(self, request, *args, **kwargs):
        self.markdown_file = 'home'
        if request.user.is_authenticated:
            self.markdown_file = 'home-user'
            
        # Redirect to roadtrip blog
        try:
            from django_static_markdown_blog.views import BlogContentView
            return BlogContentView().dispatch(request, url='road-trip', show_breadcrumb=False)
        except:
            pass
            
        return super().get(request, *args, **kwargs)


class LoginView(AlohaView):
    title = "Se connecter"
    template_name = "pages/login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('index'))

        context = self.get_context_data()
        context['next'] = request.GET.get('next', None)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):

        redirect_url = request.POST.get('next', '/')

        # Traitement des tentatives de connexions
        if request.POST.get('btnConnexion') is not None:
            username = request.POST.get('connect_user')
            password = request.POST.get('connect_password')

            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                logger.debug("User {} is connected".format(username))

                redirect_url = request.GET.get('next', redirect_url)

        elif request.POST.get('btnDeconnexion') is not None:
            logger.debug("Logout user...")
            logout(request)

        logger.debug("Redirect to {}".format(redirect_url))
        return HttpResponseRedirect(redirect_url)


class ProfilView(AlohaProtectedView):
    title = 'Votre profil'
    template_name = "pages/profil.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if request.user.is_authenticated and hasattr(request.user, 'ldap_user'):
            context['groups'] = sorted(request.user.ldap_user.group_names)

        if 'error' in request.session:
            context['error'] = request.session['error']
            del request.session['error']

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):

        # Si on n'a pas demandé à changer de password, on transmet la requête au parent
        if request.POST.get('btnChangePassword') is None:
            return super().post(request, *args, **kwargs)

        old_password = request.POST.get('txtOldPassword')
        new_password = request.POST.get('txtNewPassword')
        new_bis_password = request.POST.get('txtNewPasswordBis')

        if not old_password or not new_password or not new_bis_password:
            request.session['error'] = "Il faut remplir les trois champs pour pouvoir changer son mot de passe !"

        elif new_password != new_bis_password:
            request.session['error'] = "Le nouveau mot de passe n'est pas répété correctement !"

        else:
            try:
                logger.debug("Tentative de changement de password")
                request.user.change_password(old_password, new_password)
            except Exception as e:
                request.session['error'] = str(e)

        return HttpResponseRedirect(request.path)
