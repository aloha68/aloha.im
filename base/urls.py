from base.views import AlohaMarkdownView, IndexView, ProfilView, LoginView
from django.urls import path

urlpatterns = [
    path(r'aide', AlohaMarkdownView.as_view(markdown_file='aide'), name='aide'),
    path(r'profil', ProfilView.as_view(), name='profil'),
    path(r'login', LoginView.as_view(), name='login'),
    path(r'logout', LoginView.as_view(), name='logout'),
    path(r'', IndexView.as_view(), name='index'),
]