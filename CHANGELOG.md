# Changelog
All notable changes to this project will be documented in this file.

## Unreleased

### Changed
- Add all icons to git repository
- Update bootstrap to 4.3.1

## 0.1.0 - 2019-06-21

### Added
- Add django-roadtrip 0.1.0

### Changed
- Update django to 2.2
- Update python to 3.5
- Update django-ping-me to 0.1.0
- Update django-markdown-messaging to 0.1.0
- Update django-static-markdown-blog to 0.1.0

## 0.0.2 - 2019-04-27

### Changed
- Update django-static-markdown-blog to 0.0.2
- Use [poetry](https://github.com/sdispater/poetry) as python dependency manager

## 0.0.1 - 2018-08-20
First release
