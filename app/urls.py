from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('notes/', include(('django_static_markdown_blog.urls', 'blog'))),
    path('ping-me/', include(('django_ping_me.urls', 'ping_me'))),
    path('msg/', include(('django_markdown_messaging.urls', 'messaging'))),
    path('roadtrip/', include(('django_roadtrip.urls', 'roadtrip'))),
    path('robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow: /", content_type="text/plain")),
    path('', include('base.urls')),
]
