## Introduction

This is the source code of [aloha.im](https://aloha.im)

## Installation

To develop this website, I used the [poetry](https://github.com/sdispater/poetry) software.

    git clone git@gitlab.com:aloha68/aloha.im.git
    poetry install
    poetry run python manage.py migrate
    poetry run python manage.py runserver

## Credit

- Icon pack: <https://www.fatcow.com/free-icons>
