#!/usr/bin/env python3

from blog.core import BlogBuilder

import sys
import logging
logger = logging.getLogger(__name__)


def afficher_contenu(content):
    """Fonction d'affichage des enfants du blog"""

    print("| {} {} ({}: {})".format(
        content.depth * '| ', content.title, content.num, content.url))

    if content.previous:
        print("Précédent: {}".format(content.previous.title))

    if content.next:
        print("Suivant: {}".format(content.next.title))

    if content.children:
        for child in content.children:
            afficher_contenu(child)


def test_content_recursively(content):

    error = ''

    if error:
        logger.error("{}: {}".format(content.url, error))
    for child in content.children:
        test_content_recursively(child)


def test_markdown_reader(path):
    from aloha.markdown import MarkdownFileReader
    md = MarkdownFileReader(path).get()
    print(md.metadata)
    print(md.html)


if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)

    #test_markdown_reader('/home/thomas/Nextcloud/Blog/main.md')
    #test_markdown_reader('/home/thomas/Nextcloud/Blog/Personnel/Écrits/Fable ou histoire.md')

    builder = BlogBuilder('/home/thomas/Nextcloud/Blog/', 'main.md')
    contenu = builder.get()
    afficher_contenu(contenu)

    sys.exit()

    for path in [
        'personnel',
        'personnel/ecrits',
        'personnel/ecrits/fable-ou-histoire',
    ]:
        content = builder.get(path)
        if content:
            afficher_contenu(content)
